<hr/>
Zadanie 1:
Bazując na kontrolerze Home utworzyć zakładkę
<ul>
<li>o nas - pod urlem "/about-us" (utworzyć template dla aboutUs.html)</li>
<li>aktualności - pod urlem "/news" (utworzyć template dla news.html))</li>
</ul>
<hr/>
<p>Artykuły:</p>
<ul>
<li>https://www.baeldung.com/thymeleaf-iteration</li>
</ul>
<hr/>
<p>Zadanie 3: Bazując na HomeController i home.html wykonać:</p>
<ul>
<li>podgląd newsów w zakładce /news (lista stringów - tytuły newsów)</li>
<li>możliwość usunięcia konkretnego newsa (przycisk delete)</li>
<li>możliwość dodania nowego newsa na koniec (formularz dodawania)</li>
<li>możliwość edycji danego newsa</li>
</ul>
<hr/>
<p>Zadanie 4: Bazując na HomeController, home.html i User class:</p>
<ul>
<li>utworzyć klasę News (3 pola - id (Integer), tytuł, treść (String))</li>
<li>przebudować listę news aby nie była listą Stringów tylko obiektów News</li>
<li>poprawić news.html aby mógł zaprezentować tytuł i opis newsa</li>
<li>poprawić usuwanie newsa aby bazwało na id obiektu</li>
<li>rozszerzyć formularz dodawania newsa aby można było wpisać tytuł i treść</li>
<li>poprawić endponint w kontrolerze aby przechwycić dane dodawania newsa i zapisać je do listy</li>
<li>poprawić formularz edycji newsa</li>
<li>pamiętać o walidacji formularza dodawania/edycji newsa</li>
<li>poprawić endpoint edytowania newsa</li>
<li>* - uznajemy, że artykuły mogą się powtarzać (treść i tytuł)</li>
</ul>
<hr/>
<p>Zadanie 5: Bazując na HomeController, addUser.html i editUser.html:</p>
<ul>
<li>Utworzyć plik addNews.html i przenieść do niego formularz dodawania newsa z news.html</li>
<li>z news.html usunąć formularz dodawania a w zamian dodać przycisk 'add new news'</li>
<li>przycisk ten będzie linkiem otwierającym html z dodawaniem nowego newsa</li>
<li>utworzyć endpoint pozwalający na wyświetlanie formularza dodawania newsa</li>
<li>Utworzyć plik editNews.html i przenieść do niego formularz edycji newsa z news.html</li>
<li>z news.html usunąć formularz edycji a w zamian dodać przycisk 'edit'</li>
<li>przycisk ten będzie linkiem otwierającym html z edycją nowego newsa</li>
<li>utworzyć endpoint pozwalający na wyświetlanie formularza edycji newsa</li>
<li>* - pamiętać o dostarczeniu obiektu newsa do formularza edycji</li>
</ul>
<hr/>
<p> !!! HomeController został zmieniony na UserController (home.html -> user.html) !!!</p>
<hr/>
<p>Zadanie 6:</p>
<ul>
<li>Utworzyć service NewsService wzorując się na UserService</li>
<li>Przenieść do niego wszystkie funkcje bazujące na array liście news</li>
</ul>
<hr/>
<p>Zadanie 7: Na wzór UserController i UserService</p>
<ul>
<li>Adnotować serwis NewsService jako bean springowy</li>
<li>Wstrzyknąć NewsService do NewsController</li>
</ul>
<hr/>
<p>Zadanie 8: Na wzór UserController, UserService, user.html</p>
<ul>
<li>Dodać wyszukiwarkę w pliku news.html</li>
<li>Odebrać parametr "q" w NewsController</li>
<li>Dodać definicję funkcji wyszukiwania w UserService</li>
<li>Dodać implementację wyszukiwania w UserServiceImpl</li>
</ul>
<hr/>
<p>Zadanie 9: na podstawie UserDatabaseServiceImpl, UserRepository, User</p>
<ul>
<li>jeżeli nie posiadasz - utwórz bazę danych (w ramach np xampp) np. o nazwie db4</li>
<li>zmodyfikuj plik application.properties zgodnie z userem i hasłem do bazy na Twojej maszynie</li>
<li>z klasy News utórz encję (tabelę bazodanową (wymagane adnotacje, patrz User))</li>
<li>utworzyć NewsRepository (tabela News)</li>
<li>uwtorzyć NewsDatabaseServiceImpl</li>
<li>zmienić nazwę NewsServiceImpl na NewsArrayListServiceImpl</li>
<li>NewsDatabaseServiceImpl powinien być głównym beanem (primary)</li>
<li>do NewsDatabaseServiceImpl wstrzyknąć NewsRepository</li>
<li>w NewsDatabaseServiceImpl ma implementować interface NewsService</li>
<li>oprogramować funckje w NewsDatabaseServiceImpl</li>
<li>w NewsRepository dodać definicję metody pozwalającą na wyszukiwanie po słowach kluczowych (do zapytania potrzebne nazwy pól z tabeli News a nie User)</li>
</ul>
<hr/>
<p>Artykuły:</p>
<ul>
<li>https://spring.io/guides/gs/securing-web/</li>
<li>https://www.baeldung.com/spring-security-method-security</li>
<li>https://www.baeldung.com/spring-security-thymeleaf</li>
<li>https://www.baeldung.com/csrf-thymeleaf-with-spring-security</li>
<li>https://www.javadevjournal.com/spring/spring-security-userdetailsservice/</li>
</ul>
<hr/>
<p>Zmiana snake case na camel case w mysql: <strong>spring.jpa.hibernate.naming.physical-strategy = org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl</strong></p>
<hr/>
<p>Artykuły:</p>
<ul>
<li>https://www.baeldung.com/jpa-one-to-one</li>
<li>https://www.baeldung.com/hibernate-one-to-many</li>
<li>https://www.baeldung.com/hibernate-many-to-many</li>
<li>https://www.baeldung.com/jpa-cascade-types</li>
</ul>
<hr/>
<p>Zadanie 10 (domowe):</p>
<ul>
<li>Utworzyć encję Image z polami: id (Int), url (Str 255)</li>
<li>Utworzyć encję Category z polami: id (Int), name (Str 200), description (Str 2000), image (relacja o2o*)</li>
<li>Utworzyć encję Product z polami: id (Int), name (Str 200), description (Str 2000), category (relacja m2m*), image (relacja o2m*)</li>
<li>Utworzyć repozytoria dla Product, Category, Image</li>
<li>Utworzyć kontroler dla Product i Category</li>
<li>Utworzyć html wizualizujący listę kategorii i podłączyć go pod kontroler</li>
<li>Utworzyć html wizualizujący listę produktów należących do danej kategorii i podłączyć pod kontroler (własne zapytanie w repozytorium findAllByCategoryId)</li>
<li>Utworzyć html wizualizujący sczegóły pojedynczego produktu i podłączyć pod kontroler (wizualizujemy pola bez id)</li>
<li>Flow ma być następujące: wchodzimy na stronę i w zakładce categories widzimy listę wszystkich kategorii</li>
<li>Lista jest klikalnym linkiem - klikamy w link i przenosimy się zakładki categories/{categoryId}</li>
<li>W zakładce widzimy listę produktów należących do danej kategorii - lista również jest klikalnym linkiem (products/{productId})</li>
<li>Po przekierowaniu zobaczymy szczegóły pojedyńczego produktu</li>
<li>* - relacja o2o (one to one), o2m (one to many), m2m (many to many)</li>
</ul>
<hr/>
<p>Artykuły:</p>
<ul>
<li>https://www.baeldung.com/spring-email</li>
<li>https://www.baeldung.com/spring-web-flash-attributes</li>
</ul>
<hr/>
<p>Zadanie 11:</p>
<ul>
<li>na podstawie encji News dodać walidację do pozostałych encji respektując rozmiar kolumn w bazie</li>
</ul>
<hr/>
<p>Zadanie 12:</p>
<ul>
<li>na podstawie walidacji w encji news i sprawdzania walidacji w kontrolerze (dodawanie, edycja newsa)</li>
<li>oprogramować walidację formularza użytkownika (dodawanie, walidacja)</li>
<li>pamiętać o przekazaiu informacji jaki błąd wystąpił oraz o pokazaniu na formularzy poprzednich danych formularza</li>
<li>pamiętać o adnotacji Valid, BindingResults i RedirectAttributes</li>
</ul>
<hr/>
<p>Zadanie 13: Utwórz Rest CRUD dla Product</p>
<ul>
<li>ProductRestController</li>
<li>inject ProductService</li>
<li>getProducts (lista produktów)</li>
<li>getProduct (pojedynczy po id)</li>
<li>addProduct</li>
<li>editProduct</li>
<li>deleteProduct</li>
</ul>
<hr/>
<p>Artykuły:</p>
<ul>
<li>https://projectlombok.org/features/</li>
<li>https://www.baeldung.com/spring-boot-actuators</li>
<li>https://www.baeldung.com/spring-boot-h2-database</li>
</ul>
<hr/>
<p>
// Zapytanie HTTP typu POST zmiejący poziom logowania na INFO na uruchomionej aplikacji<br/>
// ważne aby /actuator/loggers był wystawiony (management.endpoints.web.exposure.include)<br/>
// oraz musimy wykonać zapytanie jako użytkownik ADMIN -> ..."/actuator/**").hasRole("ADMIN")<br/>
<br/>
POST /actuator/loggers/ROOT HTTP/1.1<br/>
Host: localhost:8085<br/>
Content-Type: application/json<br/>
Cookie: JSESSIONID=...<br/>
<br/>
{ "configuredLevel": "INFO" }<br/>
</p>
<hr/>
<p>Zadanie domowe:</p>
<p>Napisać unit testy dla:</p>
<ul>
<li>NewsDatabaseServiceImpl</li>
<li>ProductDatabaseServiceImpl</li>
<li>NewsArrayListServiceImpl</li>
</ul>
<p>Napisać testy integracyjne (dla ambitnych) dla:</p>
<ul>
<li>ProductRestController (wzór NewsRestController)</li>
</ul>
<hr/>
<p>Artykuły:</p>
<ul>
<li>https://attacomsian.com/blog/http-requests-resttemplate-spring-boot</li>
<li>https://developers.przelewy24.pl/index.php?pl</li>
<li>https://developer.paypal.com/docs/payouts/standard/integrate-api/</li>
<li>https://www.baeldung.com/manually-set-user-authentication-spring-security</li>
<li>https://www.baeldung.com/java-dto-pattern</li>
</ul>
<hr/>
