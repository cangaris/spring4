SET
@PREVIOUS_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS;
SET
FOREIGN_KEY_CHECKS = 0;


LOCK
TABLES `address` WRITE;
TRUNCATE `address`;
ALTER TABLE `address` DISABLE KEYS;
REPLACE
INTO `address` (`id`, `city`, `country`, `flat_nr`, `street`, `street_nr`, `zip_code`) VALUES
	(1,'Warszawa','PL',NULL,'Marszałkowska','4','00-000'),
	(2,'Kraków','PL',NULL,'Gdańska','10','01-001'),
	(3,'Poznań','PL',NULL,'Krakowska','4A','02-002');
ALTER TABLE `address` ENABLE KEYS;
UNLOCK
TABLES;


LOCK
TABLES `product` WRITE;
TRUNCATE `product`;
ALTER TABLE `product` DISABLE KEYS;
REPLACE
INTO `product` (`id`, `description`, `name`) VALUES
	(1,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
\n
\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry
\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n\n','iphone X'),
	(2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
\n
\n','iphone 11
'),
	(3,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry
\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n\n','iphone 13'),
	(4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
\n
\n','macbook pro'),
	(5,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry
\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n\n','macbook air'),
	(6,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
\n
\n','iMac');
ALTER TABLE `product` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `image` WRITE;
TRUNCATE `image`;
ALTER TABLE `image` DISABLE KEYS;
REPLACE INTO `image` (`id`, `url`, `product_id`) VALUES 
	(1,'/images/iphone14pro.jpeg',NULL),
	(2,'https://lantre.pl/media/webp_image/catalog/product/cache/0f7431f5367b37c067ffd8dcd36cc722/a/p/apple-macbook-pro-13-3-cala-m2-8-core-cpu-10-core-gpu-24gb-ram-1tb-ssd-srebrny-silver-24017_2.webp',NULL),
	(3,'https://fotoforma.pl/userdata/public/gfx/128974/apple-imac-27-wyswietlacz-retina-5k-6-rdzeniowy-intel-core-i5-3%2C1-ghz-ram-8-gb-dysk-256-gb-ssd-grafika-radeon-pro-5300-z-4-gb-pamieci-gddr6_01.jpg',NULL),
	(4,'https://www.vedion.pl/pol_pl_Apple-iPhone-11-Pro-Space-Gray-64GB-Smartfon-Stan-Bardzo-Dobry-16386_5.jpg',2),
	(5,'https://idream.pl/images/detailed/61/iPhone_13_pink.png',3),
	(6,'https://idream.pl/images/detailed/61/iPhone_13_blue.png',3);
ALTER TABLE `image` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `category` WRITE;
TRUNCATE `category`;
ALTER TABLE `category` DISABLE KEYS;
REPLACE INTO `category` (`id`, `description`, `name`, `image_id`) VALUES 
	(1,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry
\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n\n','phones',1),
	(2,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
\n
\n','laptops',2),
	(3,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry
\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n\n','desktops',3),
	(4,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
\n
\n','discounted',NULL);
ALTER TABLE `category` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `news` WRITE;
TRUNCATE `news`;
ALTER TABLE `news` DISABLE KEYS;
REPLACE INTO `news` (`id`, `content`, `title`) VALUES 
	(1,'<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry
\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen boo<strong>k. It has survived not only five ce</strong>nturies, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','News 5'),
	(2,'<p style="text-align:right;">Lorem <strong>Ipsum is simply dummy text</strong> &nbsp;of the printing and typesettin<strong>g industry. Lorem Ipsum has been th</strong>e industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was pop<strong>ularised in the 1960s with the release of Letraset </strong>sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>','News 6
'),
	(3,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry
\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n\n','News 7');
ALTER TABLE `news` ENABLE KEYS;
UNLOCK
TABLES;


LOCK
TABLES `phone` WRITE;
TRUNCATE `phone`;
ALTER TABLE `phone` DISABLE KEYS;
REPLACE
INTO `phone` (`id`, `number`, `user_id`) VALUES
	(1,'+49 89759437579',1),
	(2,'+48 89759437579',2),
	(3,'+39 78686877867',2),
	(4,'+44 67686877867',3);
ALTER TABLE `phone` ENABLE KEYS;
UNLOCK
TABLES;


LOCK
TABLES `product_category` WRITE;
TRUNCATE `product_category`;
ALTER TABLE `product_category` DISABLE KEYS;
REPLACE
INTO `product_category` (`product_id`, `category_id`) VALUES
	(1,1),
	(2,1),
	(3,1),
	(4,2),
	(5,2),
	(6,3),
	(1,4),
	(2,4);
ALTER TABLE `product_category` ENABLE KEYS;
UNLOCK
TABLES;


LOCK
TABLES `user` WRITE;
TRUNCATE `user`;
ALTER TABLE `user` DISABLE KEYS;
REPLACE
INTO `user` (`id`, `email`, `first_name`, `last_name`, `password`, `role`, `details_id`) VALUES
	(1,'damian@onet.pl','Damian','Kowalski','$2a$10$L2S1ah92CvnsMuJuwUfLGO.yuDTGVovHB5UfZSRwTcIu64l5ShHz2','EDITOR',1),
	(2,'basia@onet.pl','Basia','Nowak','$2a$10$yWllvx7X2ieSbmKBfmLIjO9HNLqG7Qq5/RiJm9EbvkZPEtSbBydr2','ADMIN',2),
	(3,'kasia@onet.pl','Kasia','Kiepska','$2a$10$WLKQbx1ew9MJJsrjUPG6/uq3vyd6FToadpeA4el1gwEodGHWpk8Ge','EDITOR',3);
ALTER TABLE `user` ENABLE KEYS;
UNLOCK
TABLES;


LOCK
TABLES `user_address` WRITE;
TRUNCATE `user_address`;
ALTER TABLE `user_address` DISABLE KEYS;
REPLACE
INTO `user_address` (`user_id`, `address_id`) VALUES
	(1,1),
	(1,3),
	(2,2),
	(3,2);
ALTER TABLE `user_address` ENABLE KEYS;
UNLOCK
TABLES;


LOCK
TABLES `user_details` WRITE;
TRUNCATE `user_details`;
ALTER TABLE `user_details` DISABLE KEYS;
REPLACE
INTO `user_details` (`id`, `personal_nr`) VALUES
	(1,'673674873678'),
	(2,'952395436878'),
	(3,'952395436878');
ALTER TABLE `user_details` ENABLE KEYS;
UNLOCK
TABLES;




SET
FOREIGN_KEY_CHECKS = @PREVIOUS_FOREIGN_KEY_CHECKS;


