package pl.cansoft.spring4.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cansoft.spring4.models.News;

@Repository
public interface NewsRepository extends JpaRepository<News, Integer> {

    //        @Query("""
    //        select n from News n where n.title like %:t% or n.content like %:c%
    //        """)
    // @Param("t") @Param("c")
    List<News> findAllByTitleContainsOrContentContains(String title, String content);

    boolean existsByTitle(String title);
}
