package pl.cansoft.spring4.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cansoft.spring4.models.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    //        @Query("""
    //            select p from Product p
    //            left join p.category c
    //            where c.id = :cid
    //            """)
    List<Product> findAllByCategoryId(Integer categoryId); // @Param("cid")

    boolean existsByName(String name);
}
