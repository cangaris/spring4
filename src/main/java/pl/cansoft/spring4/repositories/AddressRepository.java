package pl.cansoft.spring4.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cansoft.spring4.models.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {
}
