package pl.cansoft.spring4.repositories;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cansoft.spring4.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    //    @Query("""
    //        select u from User u where u.firstName like %:fn% or u.lastName like %:ln%
    //        """)
    // @Param("fn") @Param("ln")
    List<User> findAllByFirstNameContainsOrLastNameContains(String firstName, String lastName);

    // HQL - Hibernate Query Language
    // działa tak samo jak findByEmail
    //    @Query("""
    //        select u from User u where u.email = :e
    //        """)
    Optional<User> findByEmail(String email); // @Param("e") String email)
}
