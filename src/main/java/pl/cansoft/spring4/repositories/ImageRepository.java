package pl.cansoft.spring4.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cansoft.spring4.models.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, Integer> {
}
