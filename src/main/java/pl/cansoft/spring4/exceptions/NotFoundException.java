package pl.cansoft.spring4.exceptions;

public class NotFoundException extends RuntimeException {
    public NotFoundException() {
        super("Object does not exist");
    }
}
