package pl.cansoft.spring4.exceptions;

// @ResponseStatus(value = HttpStatus.CONFLICT, reason = "Name is not unique")
public class ConflictException extends RuntimeException {
    public ConflictException() {
        super("Name is not unique");
    }
}
