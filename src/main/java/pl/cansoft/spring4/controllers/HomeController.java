package pl.cansoft.spring4.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
class HomeController {

    @GetMapping("/")
    String showContactPage() {
        return "home";
    }
}
