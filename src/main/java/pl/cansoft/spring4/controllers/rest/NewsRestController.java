package pl.cansoft.spring4.controllers.rest;

import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.spring4.dtos.NewsDto;
import pl.cansoft.spring4.dtos.NewsInsertDto;
import pl.cansoft.spring4.dtos.NewsUpdateDto;
import pl.cansoft.spring4.exceptions.ConflictException;
import pl.cansoft.spring4.exceptions.NotFoundException;
import pl.cansoft.spring4.mappers.NewsDtoConverter;
import pl.cansoft.spring4.models.News;
import pl.cansoft.spring4.services.news.NewsService;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/news")
class NewsRestController {

    private final NewsService newsService;
    private final NewsDtoConverter newsDtoConverter;

    /**
     * C - create - addNews
     * R - read - getNews
     * U - update - editNews
     * D - delete - deleteNews
     */
    @GetMapping
    public List<NewsDto> getNews() {
        return newsService.getNews().stream()
            .map(news -> newsDtoConverter.convert(news)) // newsDtoConverter::convert
            .toList();
    }

    @GetMapping("/{id}")
    public NewsDto getNews(@PathVariable Integer id) {
        return newsService.findNewsById(id)
            .map(news -> newsDtoConverter.convert(news))
            .orElseThrow(() -> new NotFoundException());
    }

    // create
    @PostMapping
    public NewsDto addNews(@Valid @RequestBody NewsInsertDto newsDto) {
        // trace, debug, info, warn, error
        log.debug("add news with payload: {}", newsDto);
        var news = newsDtoConverter.convert(newsDto);
        checkUniqueItem(news);
        var savedNews = newsService.addNews(news);
        return newsDtoConverter.convert(savedNews);
    }

    // update
    @PutMapping("/{id}")
    public NewsDto editNews(@PathVariable Integer id, @Valid @RequestBody NewsUpdateDto newsDto) {
        var news = newsDtoConverter.convert(newsDto);
        checkUniqueItem(news);
        var savedNews = newsService.editNews(id, news);
        return newsDtoConverter.convert(savedNews);
    }

    // delete
    @DeleteMapping("/{id}")
    public void deleteNews(@PathVariable Integer id) {
        newsService.deleteNews(id);
    }

    private void checkUniqueItem(News news) {
        var exists = newsService.existByTitle(news.getTitle());
        if (exists) {
            throw new ConflictException();
        }
    }
}
