package pl.cansoft.spring4.controllers.rest;

import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.spring4.exceptions.ConflictException;
import pl.cansoft.spring4.exceptions.NotFoundException;
import pl.cansoft.spring4.models.Product;
import pl.cansoft.spring4.services.products.ProductService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/products")
class ProductRestController {

    private final ProductService productService;

    /**
     * backend/api/products/category/2
     */
    @GetMapping("/category/{id}")
    public List<Product> getProducts(@PathVariable Integer id) {
        return productService.findProductsByCategoryId(id);
    }

    /**
     * backend/api/products/2
     */
    @GetMapping("/{id}")
    public Product getProduct(@PathVariable Integer id) {
        return productService.findById(id)
            .orElseThrow(() -> new NotFoundException());
    }

    @PostMapping
    public Product addProduct(@Valid @RequestBody Product product) {
        checkUniqueItem(product);
        return productService.addProduct(product);
    }

    @PutMapping("/{id}")
    public Product editProduct(@PathVariable Integer id, @Valid @RequestBody Product product) {
        checkUniqueItem(product);
        return productService.editProduct(id, product);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Integer id) {
        productService.deleteById(id);
    }

    private void checkUniqueItem(Product product) {
        var exists = productService.existByName(product.getName());
        if (exists) {
            throw new ConflictException();
        }
    }
}
