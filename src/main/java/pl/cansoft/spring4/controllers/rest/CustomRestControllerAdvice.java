package pl.cansoft.spring4.controllers.rest;

import java.time.LocalDateTime;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.cansoft.spring4.exceptions.ConflictException;
import pl.cansoft.spring4.exceptions.NotFoundException;
import pl.cansoft.spring4.models.Error;

@Slf4j
@RestControllerAdvice
class CustomRestControllerAdvice {

    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error catchError(BindException exception) {
        var errorMessages = exception.getAllErrors().stream()
            .map(error -> error.getDefaultMessage())
            .toList();
        var now = LocalDateTime.now();
        var code = HttpStatus.BAD_REQUEST;
        var statusCode = code.value();
        var statusReason = code.getReasonPhrase();
        var error = new Error(errorMessages, now, statusCode, statusReason);
        log.error("Error from app: {}", error);
        return error;
    }

    @ExceptionHandler(ConflictException.class)
    public ResponseEntity<Error> catchError(ConflictException exception) {
        return formatError(exception, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Error> catchError(NotFoundException exception) {
        return formatError(exception, HttpStatus.NOT_FOUND);
    }

    private ResponseEntity<Error> formatError(RuntimeException exception, HttpStatus code) {
        var errorMessages = exception.getMessage();
        var now = LocalDateTime.now();
        var statusCode = code.value();
        var statusReason = code.getReasonPhrase();
        var error = new Error(List.of(errorMessages), now, statusCode, statusReason);
        log.error("Error from app: {}", error);
        return ResponseEntity.status(code).body(error);
    }
}
