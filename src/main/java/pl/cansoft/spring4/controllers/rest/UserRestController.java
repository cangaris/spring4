package pl.cansoft.spring4.controllers.rest;

import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.spring4.exceptions.NotFoundException;
import pl.cansoft.spring4.models.User;
import pl.cansoft.spring4.services.users.UserService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/users")
class UserRestController {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;

    record LoginData(
        @NotBlank String username,
        @NotBlank String password) {
    }

    @PostMapping("/login")
    public void login(@Valid @RequestBody LoginData loginData, HttpServletRequest req) {
        try {
            var authData = new UsernamePasswordAuthenticationToken(loginData.username(), loginData.password());
            var authUser = authenticationManager.authenticate(authData);
            var sc = SecurityContextHolder.getContext();
            sc.setAuthentication(authUser);
            var session = req.getSession(true);
            session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);
        } catch (AuthenticationException e) {
            throw new UsernameNotFoundException("Bad credentials");
        }
    }

    @GetMapping("/logout")
    public void logout(HttpServletRequest req) {
        var session = req.getSession(false);
        SecurityContextHolder.clearContext();
        if (Objects.nonNull(session)) {
            session.invalidate();
        }
    }

    @GetMapping
    public List<User> getProducts(@RequestParam(required = false) String q) {
        List<User> users;
        if (q == null) {
            users = userService.getUsers();
        } else {
            users = userService.findAllByKeyword(q);
        }
        return users;
    }

    @GetMapping("/{id}")
    public User getProduct(@PathVariable Integer id) {
        return userService.findUserById(id)
            .orElseThrow(NotFoundException::new);
    }

    @PostMapping
    public User addProduct(@Valid @RequestBody User user) {
        return userService.addUser(user);
    }

    @PutMapping("/{id}")
    public User editProduct(@PathVariable Integer id, @Valid @RequestBody User product) {
        return userService.editUser(id, product);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Integer id) {
        userService.removeUserById(id);
    }
}
