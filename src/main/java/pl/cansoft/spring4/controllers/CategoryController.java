package pl.cansoft.spring4.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.cansoft.spring4.services.categories.CategoryService;
import pl.cansoft.spring4.services.products.ProductService;

@RequiredArgsConstructor
@Controller
@RequestMapping("/category")
class CategoryController {

    private static final String REDIRECT = "redirect:/category";
    private final CategoryService categoryService;
    private final ProductService productService;

    @GetMapping
    public String showCategories(Model model) {
        var categories = categoryService.findAll();
        model.addAttribute("categories", categories);
        return "categories/categories";
    }

    @GetMapping("/{id}")
    public String showCategoryById(Model model, @PathVariable Integer id) {
        var optionalCategory = categoryService.findById(id);
        if (optionalCategory.isPresent()) {
            var category = optionalCategory.get();
            var products = productService.findProductsByCategoryId(category.getId());
            model.addAttribute("products", products);
            model.addAttribute("category", category);
            return "categories/category";
        }
        return REDIRECT;
    }
}
