package pl.cansoft.spring4.controllers;

import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.cansoft.spring4.commons.Common;
import pl.cansoft.spring4.models.News;
import pl.cansoft.spring4.services.news.NewsService;

/**
 * MVC
 * M - model - jakaś klasa User
 * V - view - jakaś HTMLka do wizualiazcji listy userów
 * C - controller - klasa Javowa, która dostarcza info do widoków
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/news")
class NewsController {

    private static final String REDIRECT_NEWS = "redirect:/news";

    private final NewsService newsService;

    /**
     * /news - prawdziwy adres URL mimo że GetMapping jest pusty
     * adres url wynika z RequestMapping
     */
    @GetMapping
    String showNewsPage(Model model, @RequestParam(required = false) String q) {
        List<News> news;
        if (q == null) {
            news = newsService.getNews();
        } else {
            news = newsService.findAllByKeyword(q);
        }
        model.addAttribute("q", q);
        model.addAttribute("list", news);
        return "news/news";
    }

    @Secured({Common.ADMIN, Common.EDITOR})
    @GetMapping("/add-form")
    String showAddNewsForm() {
        return "news/addNews";
    }

    @Secured({Common.ADMIN, Common.EDITOR})
    @GetMapping("/edit-form/{newsId}")
    String showEditNewsForm(@PathVariable Integer newsId, Model model) {
        newsService.findNewsById(newsId)
            .ifPresent(item -> model.addAttribute("news", item));
        return "news/editNews";
    }

    /**
     * /news + /delete/{id}
     */
    @Secured({Common.ADMIN, Common.EDITOR})
    @GetMapping("/delete/{newsId}")
    String deleteNews(@PathVariable Integer newsId) {
        newsService.deleteNews(newsId);
        return REDIRECT_NEWS;
    }

    /**
     * /news + /add
     */
    @Secured({Common.ADMIN, Common.EDITOR})
    @PostMapping("/add")
    String addNews(@Valid News news, BindingResult result, RedirectAttributes attributes) {
        var url = Common.redirectIfHasErrors("news/add-form", news, result, attributes);
        if (url != null) {
            return url;
        }
        newsService.addNews(news);
        return REDIRECT_NEWS;
    }

    /**
     * /news + /edit/{newsId}
     */
    @Secured({Common.ADMIN, Common.EDITOR})
    @PostMapping("/edit/{newsId}")
    String editNews(@PathVariable Integer newsId, @Valid News editedNews, BindingResult result,
                    RedirectAttributes attributes) {
        var url = Common.redirectIfHasErrors("news/edit-form/" + newsId, editedNews, result, attributes);
        if (url != null) {
            return url;
        }
        newsService.editNews(newsId, editedNews);
        return REDIRECT_NEWS;
    }
}
