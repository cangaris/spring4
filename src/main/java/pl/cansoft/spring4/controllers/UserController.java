package pl.cansoft.spring4.controllers;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.cansoft.spring4.commons.Common;
import pl.cansoft.spring4.models.User;
import pl.cansoft.spring4.models.UserRole;
import pl.cansoft.spring4.services.users.UserService;

/**
 * CRUD
 * C - create (/add-user)
 * R - read (/)
 * U - update (/edit-user/**)
 * D - delete (/delete/**)
 */
@RequiredArgsConstructor
@Controller
@RequestMapping("/user")
class UserController {

    private static final String REDIRECT = "redirect:/user";
    /**
     * DI - dependency injection (wstrzykiwanie zależności - wzorzec projektowy)
     */
    private final UserService userService;

    @Secured({Common.ADMIN, Common.EDITOR})
    @GetMapping
    String showUserPage(Model model, @RequestParam(required = false) String q) {
        List<User> users;
        if (q == null) {
            users = userService.getUsers();
        } else {
            users = userService.findAllByKeyword(q);
        }
        model.addAttribute("q", q);
        model.addAttribute("list", users);
        return "user/user";
    }

    @Secured(Common.ADMIN)
    @GetMapping("/add-form")
    String showAddUserForm(Model model) {
        model.addAttribute("roles", UserRole.values());
        return "user/addUser";
    }

    @Secured(Common.ADMIN)
    @GetMapping("/edit-form/{userId}")
    String showEditUserForm(@PathVariable Integer userId, Model model) {
        model.addAttribute("roles", UserRole.values());
        userService.findUserById(userId)
            .ifPresent(user -> model.addAttribute("user", user));
        return "user/editUser";
    }

    @Secured(Common.ADMIN)
    @GetMapping("/delete/{userId}")
    String deleteUsername(@PathVariable Integer userId) {
        userService.removeUserById(userId);
        return REDIRECT;
    }

    @Secured(Common.ADMIN)
    @PostMapping("/add")
    String addUser(User newUser) {
        userService.addUser(newUser);
        return REDIRECT;
    }

    @Secured(Common.ADMIN)
    @PostMapping("/edit/{userId}")
    String editUser(@PathVariable Integer userId, User editedUser) {
        userService.editUser(userId, editedUser);
        return REDIRECT;
    }
}
