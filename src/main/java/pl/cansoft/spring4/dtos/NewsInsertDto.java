package pl.cansoft.spring4.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Builder;
import lombok.Getter;

/**
 * DTO - Data Transfer Object
 */
@Getter
@Builder
public class NewsInsertDto {
    @NotBlank(message = "Title is required")
    @Size(min = 5, max = 255, message = "Title has to have from 5 to 255 chars")
    private String title;
    @NotBlank(message = "Content is required")
    @Size(min = 5, max = 2000, message = "Content has to have from 5 to 2000 chars")
    private String content;
}
