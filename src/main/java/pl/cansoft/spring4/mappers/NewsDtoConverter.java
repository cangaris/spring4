package pl.cansoft.spring4.mappers;

import org.springframework.stereotype.Component;
import pl.cansoft.spring4.dtos.NewsDto;
import pl.cansoft.spring4.dtos.NewsInsertDto;
import pl.cansoft.spring4.dtos.NewsUpdateDto;
import pl.cansoft.spring4.models.News;

@Component
public class NewsDtoConverter {

    public NewsDto convert(News news) {
        return NewsDto.builder()
            .id(news.getId())
            .title(news.getTitle())
            .content(news.getContent())
            .build();
    }

    public News convert(NewsDto dto) {
        return News.builder()
            .id(dto.getId())
            .title(dto.getTitle())
            .content(dto.getContent())
            .build();
    }

    public News convert(NewsUpdateDto dto) {
        return News.builder()
            .id(dto.getId())
            .title(dto.getTitle())
            .content(dto.getContent())
            .build();
    }

    public News convert(NewsInsertDto dto) {
        return News.builder()
            .title(dto.getTitle())
            .content(dto.getContent())
            .build();
    }
}
