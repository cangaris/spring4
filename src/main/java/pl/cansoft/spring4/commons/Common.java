package pl.cansoft.spring4.commons;

import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class Common {
    public static final String ADMIN = "ROLE_ADMIN";
    public static final String EDITOR = "ROLE_EDITOR";

    public static String redirectIfHasErrors(String url, Object prevData, BindingResult result,
                                             RedirectAttributes attributes) {
        var errorOptional = result.getAllErrors().stream().findFirst();
        if (errorOptional.isPresent()) {
            var errorMessage = errorOptional.get().getDefaultMessage();
            attributes.addFlashAttribute("prevData", prevData);
            attributes.addFlashAttribute("validError", errorMessage);
            return "redirect:/" + url;
        }
        return null;
    }
}
