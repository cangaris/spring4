package pl.cansoft.spring4.models;

public enum UserRole {
    ADMIN, EDITOR
}
