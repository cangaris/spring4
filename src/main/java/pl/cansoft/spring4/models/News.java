package pl.cansoft.spring4.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    // @NotBlank(message = "Title is required")
    // @Size(min = 5, max = 255, message = "Title has to have from 5 to 255 chars")
    private String title;
    // @NotBlank(message = "Content is required")
    // @Size(min = 5, max = 2000, message = "Content has to have from 5 to 2000 chars")
    @Column(length = 2000)
    private String content;
}
