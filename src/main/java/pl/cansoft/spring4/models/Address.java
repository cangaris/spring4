package pl.cansoft.spring4.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 2, nullable = false)
    private String country;
    @Column(length = 10, nullable = false)
    private String zipCode;
    @Column(length = 60, nullable = false)
    private String city;
    @Column(length = 60, nullable = false)
    private String street;
    @Column(length = 10, nullable = false)
    private String streetNr;
    @Column(length = 10)
    private String flatNr;
}
