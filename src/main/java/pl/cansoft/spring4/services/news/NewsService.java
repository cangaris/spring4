package pl.cansoft.spring4.services.news;

import java.util.List;
import java.util.Optional;
import pl.cansoft.spring4.models.News;

public interface NewsService {
    List<News> getNews();

    List<News> findAllByKeyword(String keyword);

    Optional<News> findNewsById(Integer newsId);

    void deleteNews(Integer newsId);

    News addNews(News news);

    News editNews(Integer newsId, News editedNews);

    boolean existByTitle(String title);
}
