package pl.cansoft.spring4.services.news;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import pl.cansoft.spring4.exceptions.NotFoundException;
import pl.cansoft.spring4.models.News;
import pl.cansoft.spring4.repositories.NewsRepository;

@RequiredArgsConstructor
@Primary
@Service
class NewsDatabaseServiceImpl implements NewsService {

    private final NewsRepository newsRepository;

    @Override
    public List<News> getNews() {
        return newsRepository.findAll();
    }

    @Override
    public List<News> findAllByKeyword(String keyword) {
        return newsRepository.findAllByTitleContainsOrContentContains(keyword, keyword);
    }

    @Override
    public Optional<News> findNewsById(Integer newsId) {
        return newsRepository.findById(newsId);
    }

    @Override
    public void deleteNews(Integer newsId) {
        // aby usunąć bez błędu obieky po id trzeba najpietw sprawdzić czy istnieje, wpp rzucamy błąd
        if (newsRepository.existsById(newsId)) {
            newsRepository.deleteById(newsId);
        } else {
            throw new NotFoundException();
        }
    }

    @Override
    public News addNews(News news) {
        news.setId(null);
        return newsRepository.save(news);
    }

    @Override
    public News editNews(Integer newsId, News editedNews) {
        editedNews.setId(newsId);
        return newsRepository.save(editedNews);
    }

    @Override
    public boolean existByTitle(String title) {
        return newsRepository.existsByTitle(title);
    }
}
