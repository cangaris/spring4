package pl.cansoft.spring4.services.news;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import pl.cansoft.spring4.models.News;

@Service
class NewsArrayListServiceImpl implements NewsService {

    private List<News> news = new ArrayList<>();

    public NewsArrayListServiceImpl() {
        news.add(new News(1, "News 1", "Content 1"));
        news.add(new News(2, "News 2", "Content 2"));
        news.add(new News(3, "News 3", "Content 3"));
    }

    @Override
    public List<News> getNews() {
        return news;
    }

    @Override
    public List<News> findAllByKeyword(String keyword) {
        return news.stream()
            .filter(singleNews -> singleNews.getTitle().contains(keyword) || singleNews.getContent().contains(keyword))
            .collect(Collectors.toList());
    }

    @Override
    public Optional<News> findNewsById(Integer newsId) {
        return news.stream()
            .filter(item -> item.getId().equals(newsId))
            .findFirst();
    }

    @Override
    public void deleteNews(Integer newsId) {
        news.removeIf(singleNews -> singleNews.getId().equals(newsId));
    }

    @Override
    public News addNews(News news) {
        var newId = this.news.get(this.news.size() - 1).getId() + 1;
        var newNews = new News(newId, news.getTitle(), news.getContent());
        this.news.add(newNews);
        return newNews;
    }

    @Override
    public News editNews(Integer newsId, News newsData) {
        news = news.stream()
            .map(item -> {
                if (item.getId().equals(newsId)) {
                    return new News(newsId, newsData.getTitle(), newsData.getContent());
                }
                return item;
            })
            .collect(Collectors.toList());
        return null; // todo
    }

    @Override
    public boolean existByTitle(String title) {
        return false; // todo
    }
}
