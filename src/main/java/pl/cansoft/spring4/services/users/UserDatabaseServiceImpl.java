package pl.cansoft.spring4.services.users;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.cansoft.spring4.models.Address;
import pl.cansoft.spring4.models.Phone;
import pl.cansoft.spring4.models.User;
import pl.cansoft.spring4.models.UserDetails;
import pl.cansoft.spring4.models.UserRole;
import pl.cansoft.spring4.repositories.AddressRepository;
import pl.cansoft.spring4.repositories.UserRepository;

@RequiredArgsConstructor
@Primary
@Service
class UserDatabaseServiceImpl implements UserService {

    private final AddressRepository addressRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<User> findAllByKeyword(String keyword) {
        return userRepository.findAllByFirstNameContainsOrLastNameContains(keyword, keyword);
    }

    @Override
    public Optional<User> findUserById(Integer userId) {
        return userRepository.findById(userId);
    }

    @Override
    public void removeUserById(Integer userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public User addUser(User newUser) {
        newUser.setId(null);
        // wez stare haslo, zakoduj, wpisz userowi jako nowe haslo
        newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
        return userRepository.save(newUser); // insert into bo id jest null
    }

    @Override
    public User editUser(Integer userId, User editedUser) {
        editedUser.setId(userId);
        editedUser.setPassword(passwordEncoder.encode(editedUser.getPassword()));
        return userRepository.save(editedUser); // update bo id jest numeryczne
    }

    private void initUsersWithRelations() {
        var p1 = new Phone();
        p1.setNumber("+44 67686877867");

        var p2 = new Phone();
        p2.setNumber("+39 78686877867");

        var p3 = new Phone();
        p3.setNumber("+48 89759437579");

        var p4 = new Phone();
        p4.setNumber("+49 89759437579");

        var ud1 = new UserDetails();
        ud1.setPersonalNr("673674873678");

        var ud2 = new UserDetails();
        ud2.setPersonalNr("952395436878");

        var ud3 = new UserDetails();
        ud3.setPersonalNr("952395436878");

        var ad1 = new Address();
        ad1.setCountry("PL");
        ad1.setZipCode("00-000");
        ad1.setCity("Warszawa");
        ad1.setStreet("Marszałkowska");
        ad1.setStreetNr("4");
        addressRepository.save(ad1);

        var ad2 = new Address();
        ad2.setCountry("PL");
        ad2.setZipCode("01-001");
        ad2.setCity("Kraków");
        ad2.setStreet("Gdańska");
        ad2.setStreetNr("10");
        addressRepository.save(ad2);

        var ad3 = new Address();
        ad3.setCountry("PL");
        ad3.setZipCode("02-002");
        ad3.setCity("Poznań");
        ad3.setStreet("Krakowska");
        ad3.setStreetNr("4A");
        addressRepository.save(ad3);

        userRepository.save(new User(null, "Damian", "Kowalski", "damian@onet.pl",
            passwordEncoder.encode("secretpass1"), UserRole.EDITOR, ud1, List.of(p4), List.of(ad1, ad3)));

        userRepository.save(new User(null, "Basia", "Nowak", "basia@onet.pl",
            passwordEncoder.encode("secretpass2"), UserRole.ADMIN, ud2, List.of(p3, p2), List.of(ad2)));

        userRepository.save(new User(null, "Kasia", "Kiepska", "kasia@onet.pl",
            passwordEncoder.encode("secretpass3"), UserRole.EDITOR, ud3, List.of(p1), List.of(ad2)));
    }
}
