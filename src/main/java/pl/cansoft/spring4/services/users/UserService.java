package pl.cansoft.spring4.services.users;

import java.util.List;
import java.util.Optional;
import pl.cansoft.spring4.models.User;

public interface UserService {

    List<User> getUsers();

    List<User> findAllByKeyword(String keyword);

    Optional<User> findUserById(Integer userId);

    void removeUserById(Integer userId);

    User addUser(User newUser);

    User editUser(Integer userId, User editedUser);
}
