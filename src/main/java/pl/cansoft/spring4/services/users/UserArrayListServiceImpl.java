package pl.cansoft.spring4.services.users;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import pl.cansoft.spring4.models.User;

@Service // bean
class UserArrayListServiceImpl implements UserService {

    private List<User> users = new ArrayList<>();

    public UserArrayListServiceImpl() {
        users.add(new User(7, "Damian", "Kowalski"));
        users.add(new User(10, "Basia", "Nowak"));
        users.add(new User(13, "Kasia", "Kiepska"));
    }

    @Override
    public List<User> getUsers() {
        return users;
    }

    @Override
    public List<User> findAllByKeyword(String keyword) {
        return users.stream()
            .filter(user -> user.getFirstName().contains(keyword) || user.getLastName().contains(keyword))
            .collect(Collectors.toList());
    }

    @Override
    public Optional<User> findUserById(Integer userId) {
        return users.stream()
            .filter(user -> user.getId().equals(userId))
            .findFirst();
    }

    @Override
    public void removeUserById(Integer userId) {
        users.removeIf(user -> user.getId().equals(userId));
    }

    @Override
    public User addUser(User newUser) {
        var newId = users.get(users.size() - 1).getId() + 1;
        var savedUser = new User(newId, newUser.getFirstName(), newUser.getLastName());
        users.add(savedUser);
        return savedUser;
    }

    @Override
    public User editUser(Integer userId, User editedUser) {
        users = users.stream()
            .map(user -> {
                if (user.getId().equals(userId)) {
                    return new User(user.getId(), editedUser.getFirstName(), editedUser.getLastName());
                }
                return user;
            })
            .collect(Collectors.toList());
        return null;
    }
}
