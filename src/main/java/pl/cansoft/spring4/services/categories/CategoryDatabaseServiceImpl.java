package pl.cansoft.spring4.services.categories;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.cansoft.spring4.models.Category;
import pl.cansoft.spring4.repositories.CategoryRepository;

@RequiredArgsConstructor
@Service
class CategoryDatabaseServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Optional<Category> findById(Integer id) {
        return categoryRepository.findById(id);
    }
}
