package pl.cansoft.spring4.services.categories;

import java.util.List;
import java.util.Optional;
import pl.cansoft.spring4.models.Category;

public interface CategoryService {
    List<Category> findAll();

    Optional<Category> findById(Integer id);
}
