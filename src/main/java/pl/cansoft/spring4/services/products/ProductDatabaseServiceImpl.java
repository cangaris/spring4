package pl.cansoft.spring4.services.products;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.cansoft.spring4.exceptions.NotFoundException;
import pl.cansoft.spring4.models.Product;
import pl.cansoft.spring4.repositories.ProductRepository;

@RequiredArgsConstructor
@Service
class ProductDatabaseServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public List<Product> findProductsByCategoryId(Integer categoryId) {
        return productRepository.findAllByCategoryId(categoryId);
    }

    @Override
    public Optional<Product> findById(Integer productId) {
        return productRepository.findById(productId);
    }

    @Override
    public void deleteById(Integer productId) {
        if (productRepository.existsById(productId)) {
            productRepository.deleteById(productId);
        } else {
            throw new NotFoundException();
        }
    }

    @Override
    public Product addProduct(Product product) {
        product.setId(null);
        return productRepository.save(product);
    }

    @Override
    public Product editProduct(Integer productId, Product product) {
        product.setId(productId);
        return productRepository.save(product);
    }

    @Override
    public boolean existByName(String name) {
        return productRepository.existsByName(name);
    }
}
