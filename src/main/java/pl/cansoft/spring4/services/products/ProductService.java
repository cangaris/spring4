package pl.cansoft.spring4.services.products;

import java.util.List;
import java.util.Optional;
import pl.cansoft.spring4.models.Product;

public interface ProductService {
    List<Product> findProductsByCategoryId(Integer categoryId);

    Optional<Product> findById(Integer productId);

    void deleteById(Integer productId);

    Product addProduct(Product product);

    Product editProduct(Integer productId, Product product);

    boolean existByName(String name);
}
