package pl.cansoft.spring4.services.categories;

import java.util.List;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.cansoft.spring4.models.Category;
import pl.cansoft.spring4.repositories.CategoryRepository;

// Mockito (biblioteka do mockowania - tj. tworzenia wydmuszek obiektów)
// JUnit (bliblioteka do uruchamiania i sprawdzania testów)
@ExtendWith(MockitoExtension.class)
class CategoryDatabaseServiceImplTest {

    @InjectMocks
    CategoryDatabaseServiceImpl service;
    @Mock
    CategoryRepository categoryRepository;

    @DisplayName("should call find all from repo")
    @Test
    void shouldCallFindAllFromRepo() {
        // given
        var categoryId = 100;
        List<Category> list = List.of(Category.builder()
            .id(categoryId)
            .build());
        // when
        Mockito.when(categoryRepository.findAll()).thenReturn(list);
        var results = service.findAll();
        // then
        Mockito.verify(categoryRepository).findAll();
        Assertions.assertThat(results.size()).isEqualTo(1);
        Assertions.assertThat(results.get(0).getId()).isEqualTo(categoryId);
    }

    @DisplayName("should call find by id from repo when repo returns object")
    @Test
    void shouldCallFindByIdFromRepoWhenReturnsObj() {
        // given
        var categoryId = 17;
        var optCategory = Optional.of(Category.builder()
            .id(categoryId)
            .build());
        // when
        Mockito.when(categoryRepository.findById(categoryId)).thenReturn(optCategory);
        var results = service.findById(categoryId);
        // then
        Mockito.verify(categoryRepository).findById(categoryId);
        Assertions.assertThat(results).isPresent();
        Assertions.assertThat(results.get().getId()).isEqualTo(categoryId);
    }

    @DisplayName("should call find by id from repo when repo does not return object")
    @Test
    void shouldCallFindByIdFromRepoWhenDoesNotReturnObj() {
        // given
        var categoryId = 17;
        Optional<Category> optCategory = Optional.empty();
        // when
        Mockito.when(categoryRepository.findById(categoryId)).thenReturn(optCategory);
        var results = service.findById(categoryId);
        // then
        Mockito.verify(categoryRepository).findById(categoryId);
        Assertions.assertThat(results).isEmpty();
    }
}
