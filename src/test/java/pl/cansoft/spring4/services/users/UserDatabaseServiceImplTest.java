package pl.cansoft.spring4.services.users;

import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.cansoft.spring4.models.User;
import pl.cansoft.spring4.repositories.AddressRepository;
import pl.cansoft.spring4.repositories.UserRepository;

@Slf4j
@ExtendWith(MockitoExtension.class)
class UserDatabaseServiceImplTest {

    @InjectMocks
    UserDatabaseServiceImpl service;
    @Mock
    AddressRepository addressRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    PasswordEncoder passwordEncoder;

    @DisplayName("should return saved user and call user repo and password encoder")
    @Test
    void addUser() {
        // given
        var userToSave = User.builder().build();
        var savedUser = User.builder().id(3).build();
        // when
        Mockito.when(userRepository.save(userToSave)).thenReturn(savedUser);
        var resp = service.addUser(userToSave);
        // then
        Mockito.verify(userRepository).save(userToSave);
        Mockito.verify(passwordEncoder).encode(userToSave.getPassword());
        Assertions.assertThat(resp.getId()).isEqualTo(savedUser.getId());
    }

    @DisplayName("should return saved user and call user repo and password encoder")
    @Test
    void editUser() {
        // given
        var currentUserId = 4;
        var userToSave = User.builder().id(currentUserId).build();
        var savedUser = User.builder().id(currentUserId).build();
        // when
        Mockito.when(userRepository.save(userToSave)).thenReturn(savedUser);
        var resp = service.editUser(currentUserId, userToSave);
        // then
        Mockito.verify(userRepository).save(userToSave);
        Mockito.verify(passwordEncoder).encode(userToSave.getPassword());
        Assertions.assertThat(resp.getId()).isEqualTo(savedUser.getId());
    }

    @BeforeEach
    void setUp() {
        log.info("BeforeEach");
    }

    @AfterEach
    void tearDown() {
        log.info("AfterEach");
    }
}
