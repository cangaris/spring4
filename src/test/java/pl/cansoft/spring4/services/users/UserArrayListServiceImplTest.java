package pl.cansoft.spring4.services.users;

import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.cansoft.spring4.models.User;

@Slf4j
@ExtendWith(MockitoExtension.class)
class UserArrayListServiceImplTest {

    @InjectMocks
    UserArrayListServiceImpl service;

    @DisplayName("should call getUsers and receive list size 3 with first object name equals to Damian")
    @Test
    void shouldReceiveListWithSize3() {
        // given
        // when
        var list = service.getUsers();
        // then
        // Assertions.assertThat(list.size()).isEqualTo(3); to samo co niżej
        Assertions.assertThat(list).hasSize(3);
        Assertions.assertThat(list.get(0).getFirstName()).isEqualTo("Damian");
    }

    @DisplayName("should find object by keyword for existing object")
    @Test
    void shouldFindObjectByKeywordForExistingObject() {
        // given
        var keyword = "Basia";
        // when
        var list = service.findAllByKeyword(keyword);
        // then
        Assertions.assertThat(list).hasSize(1);
        Assertions.assertThat(list.get(0).getFirstName()).isEqualTo(keyword);
    }

    @DisplayName("should find object by keyword for non existing object")
    @Test
    void shouldFindObjectByKeywordForNonExistingObject() {
        // given
        var keyword = "Basia3";
        // when
        var list = service.findAllByKeyword(keyword);
        // then
        Assertions.assertThat(list).isEmpty(); // to samo co hasSize(0)
    }

    @DisplayName("should receive optional with found user for existing user key")
    @Test
    void shouldReceiveOptionalWithFoundUserForExistingUserKey() {
        // given
        var userId = 13;
        // when
        var resp = service.findUserById(userId);
        // then
        Assertions.assertThat(resp).isPresent();
        Assertions.assertThat(resp.get().getId()).isEqualTo(userId);
    }

    @DisplayName("should receive empty optional for non existing user key")
    @Test
    void shouldReceiveEmptyOptionalForNonExistingId() {
        // given
        var userId = 18;
        // when
        var resp = service.findUserById(userId);
        // then
        Assertions.assertThat(resp).isEmpty();
    }

    @DisplayName("should List Has Size 2 After Removing For Existing Id")
    @Test
    void shouldListHasSize2AfterRemovingForExistingId() {
        // given
        var userId = 10;
        // when
        service.removeUserById(userId);
        // then
        Assertions.assertThat(service.getUsers()).hasSize(2);
    }

    @DisplayName("should List Has Size 2 After Removing For Non Existing Id")
    @Test
    void shouldListHasSize2AfterRemovingForNonExistingId() {
        // given
        var userId = 101;
        // when
        service.removeUserById(userId);
        // then
        Assertions.assertThat(service.getUsers()).hasSize(3);
    }

    @DisplayName("should add user at the end of list, list has size 4")
    @Test
    void shouldAddUserAtTheEndOfList() {
        // given
        var user = User.builder()
            .firstName("Jan")
            .lastName("Kowalski")
            .build();
        // when
        service.addUser(user);
        // then
        var list = service.getUsers();
        var newUser = list.get(3);
        Assertions.assertThat(list).hasSize(4);
        Assertions.assertThat(newUser.getId()).isEqualTo(list.get(2).getId() + 1);
        Assertions.assertThat(newUser.getFirstName()).isEqualTo(user.getFirstName());
        Assertions.assertThat(newUser.getLastName()).isEqualTo(user.getLastName());
    }

    @DisplayName("should Edit Object By Existing Id")
    @Test
    void shouldEditObjectByExistingId() {
        // given
        var userId = 13;
        var userToSave = User.builder()
            .id(userId)
            .firstName("Waldek")
            .lastName("Kiepski")
            .build();
        // when
        service.editUser(userId, userToSave);
        // then
        var list = service.getUsers();
        var editedUser = service.findUserById(userId).get();
        Assertions.assertThat(list).hasSize(3);
        Assertions.assertThat(editedUser.getFirstName()).isEqualTo(userToSave.getFirstName());
        Assertions.assertThat(editedUser.getLastName()).isEqualTo(userToSave.getLastName());
    }

    @DisplayName("should not Edit Object By Non Existing Id")
    @Test
    void shouldNotEditObjectByNonExistingId() {
        // given
        var userId = 21;
        var userToSave = User.builder()
            .id(userId)
            .firstName("Waldek")
            .lastName("Kiepski")
            .build();
        // when
        service.editUser(userId, userToSave);
        // then
        var list = service.getUsers();
        var optionalUser = service.findUserById(userId);
        Assertions.assertThat(list).hasSize(3);
        Assertions.assertThat(optionalUser).isEmpty();
    }
}
