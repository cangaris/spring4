package pl.cansoft.spring4.integrations;

class NewsRestIntegrationTestCommon {
    static String GET_ALL_JSON = """
        [
            {
                "id": 1,
                "title": "New News Integration Test",
                "content": "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>"
            }
        ]
        """;
    static String GET_BY_ID_1_JSON = """
        {
            "id": 1,
            "title": "New News Integration Test",
            "content": "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>"
        }
        """;
    static String POST_ADD_JSON = """
        {
            "title": "New News Integration Test",
            "content": "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>"
        }
        """;
    static String PUT_ADD_JSON = """
        {
            "id": 1,
            "title": "Edited News Integration Test",
            "content": "<p>Lorem Ipsum is simply edited text of the printing and typesetting industry.</p>"
        }
        """;
    static String GET_BY_ID_1_JSON_EDITED = """
        {
            "id": 1,
            "title": "Edited News Integration Test",
            "content": "<p>Lorem Ipsum is simply edited text of the printing and typesetting industry.</p>"
        }
        """;
    static String ERROR_409_JSON = """
        {
            "message": [ "Name is not unique" ],
            "status": 409,
            "error": "Conflict"
        }
        """;
}
