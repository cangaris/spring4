package pl.cansoft.spring4.integrations;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class NewsRestControllerIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    @WithMockUser(roles = "ADMIN")
    @Order(1)
    @Test
    void testGetApiNewsById_ended404() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/news/1"))
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @WithMockUser(roles = "ADMIN")
    @Order(2)
    @Test
    void testPostApiNews() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/news")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(NewsRestIntegrationTestCommon.POST_ADD_JSON)
            )
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(NewsRestIntegrationTestCommon.GET_BY_ID_1_JSON));
    }

    @WithMockUser(roles = "ADMIN")
    @Order(3)
    @Test
    void testPostApiNews_Conflict() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/news")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(NewsRestIntegrationTestCommon.POST_ADD_JSON)
            )
            .andExpect(MockMvcResultMatchers.status().isConflict())
            .andExpect(MockMvcResultMatchers.content().json(NewsRestIntegrationTestCommon.ERROR_409_JSON));
    }

    @WithMockUser(roles = "ADMIN")
    @Order(4)
    @Test
    void testGetApiNews() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/news"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(NewsRestIntegrationTestCommon.GET_ALL_JSON));
    }

    @WithMockUser(roles = "ADMIN")
    @Order(5)
    @Test
    void testGetApiNewsById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/news/1"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(NewsRestIntegrationTestCommon.GET_BY_ID_1_JSON));
    }

    @WithMockUser(roles = "ADMIN")
    @Order(6)
    @Test
    void testPutApiNews() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/api/news/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(NewsRestIntegrationTestCommon.PUT_ADD_JSON)
            )
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(NewsRestIntegrationTestCommon.GET_BY_ID_1_JSON_EDITED));
    }

    @WithMockUser(roles = "ADMIN")
    @Order(7)
    @Test
    void testDeleteApiNewsById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/news/1"))
            .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @WithMockUser(roles = "ADMIN")
    @Order(8)
    @Test
    void testDeleteApiNewsById_ended404() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/news/1"))
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
